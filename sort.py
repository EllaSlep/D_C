def MergeSort (a):
    if len(a)==1 or len(a)==0:
        return a
    left, right = a[:len(a)//2], a[len(a)//2:]
    MergeSort(left)
    MergeSort(right)
    n=m=k=0
    c=[0]*(len(left)+len(right))
    while n<len(left) and m<len(right):
        if left[n] <=right[m]:
            c[k]=left[n]
            n+=1
        else:
            c[k]=right[m]
            m+=1
        k+=1
    while n<len(left):
        c[k]=left[n]
        n+=1
        k+=1
    while m<len(right):
        c[k]=right[m]
        m+=1
        k+=1
    for i in range (len(a)):
        a[i]=c[i]
    return a

with open ("sort.in","r") as f:
    n=int (f.readline())
    a=list(map(int,f.readline().split()))
    MergeSort(a)

with open ("sort.out","w") as f1:
    f1.write(' '.join(map(str,a)))